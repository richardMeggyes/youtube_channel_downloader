import os


def download_command(config, channel):
    return ''.join([config["YOUTUBE_DL_ARGS"]["youtube_dl_beginning"], ' ',
                    config["YOUTUBE_DL_ARGS"]["youtube_dl_quality"].format(channel.quality), ' ',
                    config["YOUTUBE_DL_ARGS"]["youtube_dl_save_data"], ' ',
                    config['YOUTUBE_DL_ARGS']['youtube_dl_download_archive'], ' ',
                    config['YOUTUBE_DL_ARGS']['youtube_dl_download_archive_path'].format(os.getcwd() + os.sep, os.sep, channel.name), ' ',
                    config["YOUTUBE_DL_ARGS"]["youtube_dl_other_switches"], ' ',
                    config["YOUTUBE_DL_ARGS"]["youtube_dl_output_file_name"].format(channel.storage_path), ' ',
                    config["YOUTUBE_DL_ARGS"]["youtube_dl_channel_to_download"].format(channel.url)])
