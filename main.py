import os
import random
import time
import downloader
import config_loader
import file_checker
import subprocess

from error_log_holder import ErrorLogHolder
import enum


def check_files(config):
    files = file_checker.get_files(config)
    bad_files = file_checker.check_size(files)
    if bad_files:
        time.sleep(10)


def create_download_directory(path_to_create):
    if not os.path.isdir(path_to_create):
        print("Creating directory", path_to_create)
        os.mkdir(path_to_create)


def create_archive_file(archive_path):
    if not os.path.isfile(archive_path):
        print("Creating archive file:", archive_path)
        with open(archive_path, 'a+') as f:
            f.write("\n")


def execute_youtube_dl(conf, chann, errorLogHolder):
    command = downloader.download_command(conf, chann)
    print("Youtube_DL cmd:", command)
    proc = subprocess.Popen(command,
                            bufsize=1, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, close_fds=True, shell=True)
    for line in iter(proc.stdout.readline, b''):
        line_str = line.decode("utf-8").rstrip()
        print(line_str)
        handle_youtube_dl_errors(line_str, errorLogHolder, chann)
    proc.stdout.close()
    proc.wait()


def handle_youtube_dl_errors(line, errorLogHolder, channel):
    if "ERROR:" in line:
        errorLogHolder.add_line(channel.name + ": " + line)
    # if "Error" in line:
    #     errorLogHolder.add_line(channel.name + ": " + line)


def get_size(start_path='.'):
    total_size = 0
    for dirpath, dirnames, filenames in os.walk(start_path):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            # skip if it is symbolic link
            if not os.path.islink(fp):
                total_size += os.path.getsize(fp)

    return total_size


# Enum for size units
class SIZE_UNIT(enum.Enum):
    BYTES = 1
    KB = 2
    MB = 3
    GB = 4


def convert_unit(size_in_bytes, unit):
    """ Convert the size from bytes to other units like KB, MB or GB"""
    if unit == SIZE_UNIT.KB:
        return size_in_bytes / 1024
    elif unit == SIZE_UNIT.MB:
        return size_in_bytes / (1024 * 1024)
    elif unit == SIZE_UNIT.GB:
        return size_in_bytes / (1024 * 1024 * 1024)
    else:
        return size_in_bytes


if __name__ == "__main__":
    errorLogHolder = ErrorLogHolder()

    print("Loading config...")
    config = config_loader.get_config()
    print("Getting channels...")
    channels = config_loader.get_channels(config)
    print("Checking files...")
    check_files(config)
    download_folder_size_after_start = get_size(config['DEFAULT']['download_path'])

    print("Starting download")
    while True:
        download_folder_size_before_download = get_size(config['DEFAULT']['download_path'])
        for channel in channels:
            print("Downloading channel: " + channel.name)
            create_download_directory(channel.storage_path)
            archive_file_path = config['YOUTUBE_DL_ARGS']['youtube_dl_download_archive_path'].format(os.getcwd() + os.sep, os.sep, channel.name)
            create_archive_file(archive_file_path)
            execute_youtube_dl(config, channel, errorLogHolder)
        check_files(config)

        errorLogHolder.print_errors()

        download_folder_size_after_download = get_size(config['DEFAULT']['download_path'])
        downloaded_size = download_folder_size_after_download - download_folder_size_before_download
        readable_downloaded_size = convert_unit(downloaded_size, SIZE_UNIT.MB)
        downloaded_size_from_start = download_folder_size_after_download - download_folder_size_after_start
        readable_downloaded_size_from_start = convert_unit(downloaded_size, SIZE_UNIT.MB)
        print("Downloaded: ", str(readable_downloaded_size), " MB")
        print("Downloaded since start: ", str(readable_downloaded_size_from_start), " MB")
        sleep_time = random.randrange(7200, 14400)
        print("Finished, waiting " + str(sleep_time) + " minutes before downloading again.")
        time.sleep(sleep_time)
