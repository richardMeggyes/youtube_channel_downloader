import os
import configparser
from channel import Channel

config_file_location = 'config.ini'


class Config:
    def __init__(self, archive_path, download_path, youtube_dl_beginning, youtube_dl_quality, youtube_dl_save_data,
                 youtube_dl_download_archive, youtube_dl_download_archive_path, youtube_dl_other_switches, youtube_dl_output_file_name,
                 youtube_dl_channel_to_download, quality_default, quality_best, quality_1920,
                 quality_1280, quality_854, quality_640, quality_426, quality_256):

        self.archive_path = archive_path
        self.download_path = download_path
        self.youtube_dl_beginning = youtube_dl_beginning
        self.youtube_dl_quality = youtube_dl_quality
        self.youtube_dl_save_data = youtube_dl_save_data
        self.youtube_dl_download_archive = youtube_dl_download_archive
        self.youtube_dl_download_archive_path = youtube_dl_download_archive_path
        self.youtube_dl_other_switches = youtube_dl_other_switches
        self.youtube_dl_output_file_name = youtube_dl_output_file_name
        self.youtube_dl_channel_to_download = youtube_dl_channel_to_download

        self.quality_default = quality_default
        self.quality_best = quality_best
        self.quality_1920 = quality_1920
        self.quality_1280 = quality_1280
        self.quality_854 = quality_854
        self.quality_640 = quality_640
        self.quality_426 = quality_426
        self.quality_256 = quality_256


def create_default_config_file():
    config = configparser.RawConfigParser()
    config['DEFAULT'] = {'download_path': '',
                         'archive_path': ''}

    config['YOUTUBE_DL_ARGS'] = {
        'youtube_dl_beginning': 'youtube-dl',
        'youtube_dl_quality': '-f {}',
        'youtube_dl_save_data': '--write-description --write-thumbnail',
        'youtube_dl_download_archive': '--no-post-overwrites --download-archive archive/downloaded_{}.txt',
        'youtube_dl_other_switches': '--cookies --no-post-overwrites --restrict-filenames -ciw',
        'youtube_dl_output_file_name': '-o "{}%(upload_date)s_%(title)s_%()s_%(display_id)s.%(ext)s"',
        'youtube_dl_channel_to_download': '-v {}'}

    config['DOWNLOAD_QUALITY'] = {
        'quality_default': "\'bestvideo[width<=640]+bestaudio/best[width<=640]\'",
        'quality_best': "\'bestvideo[ext=mp4]+bestaudio[ext=m4a]/bestvideo+bestaudio\' --merge-output-format mp4",
        'quality_1920': "\'bestvideo[width<=1920]+bestaudio/best[width<=1920]\'",
        'quality_1280': "\'bestvideo[width<=1280]+bestaudio/best[width<=1280]\'",
        'quality_854': "\'bestvideo[width<=854]+bestaudio/best[width<=854]\'",
        'quality_640': "\'bestvideo[width<=640]+bestaudio/best[width<=640]\'",
        'quality_426': "\'bestvideo[width<=426]+bestaudio/best[width<=426]\'",
        'quality_256': "\'bestvideo[width<=256]+bestaudio/best[width<=256]\'"
    }

    with open(config_file_location, 'w') as configfile:
        config.write(configfile)


def get_config():
    if not os.path.isfile(config_file_location):
        create_default_config_file()

    downloader_config = Config
    config = configparser.RawConfigParser()
    config.read('config.ini')

    download_path = config['DEFAULT']['download_path']
    if download_path == '':
        download_path = os.path.join(os.getcwd(), "downloads")
        if not os.path.isdir(download_path):
            os.mkdir(download_path)
    downloader_config.download_path = download_path

    archive_path = config['DEFAULT']['archive_path']
    if archive_path == '':
        archive_path = os.path.join(os.getcwd(), "archive")
        if not os.path.isdir(archive_path):
            os.mkdir(archive_path)
    downloader_config.download_path = archive_path

    downloader_config.youtube_dl_beginning = config['YOUTUBE_DL_ARGS']['youtube_dl_beginning']
    downloader_config.youtube_dl_quality = config['YOUTUBE_DL_ARGS']['youtube_dl_quality']
    downloader_config.youtube_dl_save_data = config['YOUTUBE_DL_ARGS']['youtube_dl_save_data']
    downloader_config.youtube_dl_download_archive = config['YOUTUBE_DL_ARGS']['youtube_dl_download_archive']
    downloader_config.youtube_dl_download_archive_path = config['YOUTUBE_DL_ARGS']['youtube_dl_download_archive_path']
    downloader_config.youtube_dl_other_switches = config['YOUTUBE_DL_ARGS']['youtube_dl_other_switches']
    downloader_config.youtube_dl_output_file_name = config['YOUTUBE_DL_ARGS']['youtube_dl_output_file_name']
    downloader_config.youtube_dl_channel_to_download = config['YOUTUBE_DL_ARGS']['youtube_dl_channel_to_download']

    downloader_config.quality_default = config['DOWNLOAD_QUALITY']['quality_default']
    downloader_config.quality_best = config['DOWNLOAD_QUALITY']['quality_best']
    downloader_config.quality_1920 = config['DOWNLOAD_QUALITY']['quality_1920']
    downloader_config.quality_1280 = config['DOWNLOAD_QUALITY']['quality_1280']
    downloader_config.quality_854 = config['DOWNLOAD_QUALITY']['quality_854']
    downloader_config.quality_640 = config['DOWNLOAD_QUALITY']['quality_640']
    downloader_config.quality_426 = config['DOWNLOAD_QUALITY']['quality_426']
    downloader_config.quality_256 = config['DOWNLOAD_QUALITY']['quality_256']
    # return downloader_config
    return config


def get_channels(config):
    channel_list = []

    channels_file = os.path.join(os.getcwd(), 'channels.ini')
    with open(channels_file, 'r') as f:
        for line in f.readlines():
            if line[0] != '#' and line.count(',') == 2:
                parsed_channel = line.split(',')
                channel = Channel(parsed_channel[0], parsed_channel[2], 0, config["DOWNLOAD_QUALITY"][parsed_channel[1]], config["DEFAULT"]["download_path"])
                channel_list.append(channel)
    return channel_list

# Notes

"""
-ciw is 3 parameter options combined
-c, --continue  [Force resume of partially downloaded files]
-i, --ignore-errors [Continue on download errors, for example to skip unavailable videos in a playlist]
-w, --no-overwrites  [Do not overwrite files]

--restrict-filenames
it makes the file name ascii character only, no white spaces and &
"""