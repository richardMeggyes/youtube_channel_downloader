from datetime import datetime
import os


class ErrorLogHolder:
    def __init__(self):
        self.line_list = []

    def add_line(self, line):
        self.line_list.append(line)

    def print_errors(self):
        print("")
        if len(self.line_list) > 0:
            for line in self.line_list:
                print(line)

            now = datetime.now()
            dt_string = now.strftime("%Y_%m_%d-%H_%M_%S-")

            error_log_dir = 'error_logs'
            error_log_file = dt_string + 'ERROR.txt'
            with open(os.path.join(error_log_dir, error_log_file), 'w+') as f:
                for line in self.line_list:
                    f.write(line + '\n')
        else:
            print("No errors during download")
            print("")
        self.line_list = []