import os
import file_tools
from datetime import datetime


def get_files(config):
    files_path = config['DEFAULT']['download_path']
    video_files = []
    for path, subdirs, files in os.walk(files_path):
        for name in files:
            file = os.path.join(path, name)
            video_files.append(file)
    return video_files


def check_size(files):
    bad_size_files = []
    for file in files:
        # print("Checking file:", file)
        if file_tools.is_video_file(file):
            size = os.path.getsize(file)
            if size == 0:
                bad_size_files.append(file)

    if len(bad_size_files) > 0:
        error_log(bad_size_files)
        print()
        for file in bad_size_files:
            print(file)
        print('Found {} files with bad size:'.format(len(bad_size_files)))
        return True
    return False


def error_log(file_list):
    now = datetime.now()
    dt_string = now.strftime("%Y_%m_%d-%H_%M_%S-")

    error_log_dir = 'error_logs'
    error_log_file = dt_string + 'bad_file_size.txt'
    error_video_ids_file = dt_string + 'bad_ids.txt'
    if not os.path.isdir(error_log_dir):
        os.mkdir(error_log_dir)

    with open(os.path.join(error_log_dir, error_log_file), 'w+')as f:
        for file in file_list:
            f.write(file + '\n')

    with open(os.path.join(error_log_dir, error_video_ids_file), 'w+')as f:
        for file in file_list:
            print(file)
            video_id = file.split('_NA_')[1].split('.')[0]
            f.write(video_id + '\n')
