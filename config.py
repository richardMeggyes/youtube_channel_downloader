import os

# media_library_path = "/media/readdeo/WDENT/_yt_854/"  # Videos will be downloaded to this path

# archive_path = os.path.join(os.getcwd(), 'archive')

# channel name, storage path, channel link
#youtube_dl_link = 'youtube-dl -f {} --write-description --write-info-json --download-archive downloaded_{}.txt --no-post-overwrites --restrict-filenames -ciw -o "{}%(upload_date)s_%(title)s_%()s_%(display_id)s.%(ext)s" -v {}'
# number of videos to download, channel name, storage path, channel link
#youtube_dl_link_limited = 'youtube-dl --playlist-items 1-{} -f {} --download-archive downloaded_{}.txt --no-post-overwrites --restrict-filenames -ciw -o "{}%(upload_date)s_%(title)s_%()s_%(display_id)s.%(ext)s" -v {}'


# Youtube-dl's arguments
# youtube_dl_beginning = 'youtube-dl'
# youtube_dl_quality = '-f {}'
# youtube_dl_save_data = '--write-description --write-thumbnail'
# youtube_dl_download_archive = '--no-post-overwrites --download-archive ' + os.path.join(archive_path, 'downloaded_{}.txt')
# youtube_dl_other_switches = '--cookies --no-post-overwrites --restrict-filenames -ciw'
# youtube_dl_output_file_name = '-o "{}%(upload_date)s_%(title)s_%()s_%(display_id)s.%(ext)s"'
# youtube_dl_channel_to_download = '-v {}'

"""
-ciw is 3 parameter options combined
-c, --continue  [Force resume of partially downloaded files]
-i, --ignore-errors [Continue on download errors, for example to skip unavailable videos in a playlist]
-w, --no-overwrites  [Do not overwrite files]

--restrict-filenames
it makes the file name ascii character only, no white spaces and &
"""
